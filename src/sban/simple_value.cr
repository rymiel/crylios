enum SBAN::SimpleValue : UInt8
  False     = 20
  True
  Null

  def to_diagnostic : String
    case self
    when False
      "false"
    when True
      "true"
    when Null
      "null"
    else
      "simple(#{self.value})"
    end
  end

  def to_t : Bool | Nil
    case self
    when False
      false
    when True
      true
    when Null
      nil
    end
  end

  def is_nil? : Bool
    case self
    when Null
      true
    else
      false
    end
  end
end
