require "./spec_helper"

module Stelios
  alias TermReason = B::ContainerAddResult::TerminationReason
  describe LinearContainer do
    describe "#add" do
      test = Inventory.new 6
      it "adds an item to the first slot" do
        expected = [{slot: 0, delta_count: 2, result_count: 2}]
        actual = test.add Item.new(name: "test", count: 2_u64, stack: 8_u64)
        actual.terminate.should eq TermReason::None
        actual.entries.should eq expected
      end
      it "adds another item to the second slot" do
        expected = [{slot: 1, delta_count: 3, result_count: 3}]
        actual = test.add Item.new(name: "bar", count: 3_u64, stack: 8_u64)
        actual.terminate.should eq TermReason::None
        actual.entries.should eq expected
      end
      it "adds a copy of the first item into the first slot again" do
        expected = [{slot: 0, delta_count: 4, result_count: 6}]
        actual = test.add Item.new(name: "test", count: 4_u64, stack: 8_u64)
        actual.terminate.should eq TermReason::None
        actual.entries.should eq expected
      end
      it "adds an overflow of the first item into the third slot" do
        expected = [{slot: 0, delta_count: 2, result_count: 8},
                    {slot: 2, delta_count: 3, result_count: 3}]
        actual = test.add Item.new(name: "test", count: 5_u64, stack: 8_u64)
        actual.terminate.should eq TermReason::None
        actual.entries.should eq expected
      end
      it "handles items way past their stack size" do
        expected = [{slot: 2, delta_count: 5, result_count: 8},
                    {slot: 3, delta_count: 8, result_count: 8},
                    {slot: 4, delta_count: 7, result_count: 7}]
        actual = test.add Item.new(name: "test", count: 20_u64, stack: 8_u64)
        actual.terminate.should eq TermReason::None
        actual.entries.should eq expected
      end
      it "reports running out of space while adding items" do
        expected = [{slot: 5, delta_count: 8, result_count: 8}]
        test_item = Item.new(name: "filler", count: 12_u64, stack: 8_u64)
        actual = test.add test_item
        test_item.count.should eq 4 # Leftover
        actual.terminate.should eq TermReason::NoRoom
        actual.entries.should eq expected
      end
      it "reports a full inventory before adding items" do
        test_item = Item.new(name: "filler", count: 3_u64, stack: 8_u64)
        actual = test.add test_item
        test_item.count.should eq 3 # Leftover
        actual.terminate.should eq TermReason::Full
        actual.entries.should be_empty
      end
    end
  end
end
