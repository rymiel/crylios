require "./container"

module Stelios
  class Item
    enum Rarity
      Common
      Uncommon
      Rare
      Epic
      Legendary
    end
    
    property count : UInt64
    property stack : UInt64
    property inventory : Container?
    property name : String
    property rarity : Rarity

    def initialize(*, @inventory = nil, @name = "unnamed item", @rarity = Rarity::Common, @count = 1_u64, @stack = 1_u64)
    end

    # def describe_name(pl : Bool = false)
    #   "#{@name}#{'s' if pl}"
    # end

    # def plural_describe_name
    #   describe_name(true)
    # end

    def_equals @name, @rarity, @stack

    def dup
      Item.new inventory: @inventory, name: @name, rarity: @rarity, count: @count, stack: @stack
    end
  end
end
