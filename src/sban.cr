require "./sban/**"

module SBAN
  VERSION = "0.1.0"

  def SBAN.diagnostic(bytes : Bytes)
    return SBAN::Diagnostic.to_s(bytes)
  end

  def SBAN.diagnostic_for(object, pretty=false, width=80)
    raise ArgumentError.new("Can't serialize #{object.class}") unless object.responds_to? :to_sban
    indices = if object.class.is_a? SBAN::FieldIndices
      object.class.sban_field_indices
    else nil
    end
    if pretty
      pp_io = ::IO::Memory.new
      printer = PrettyPrint.new pp_io, width
      SBAN::Diagnostic.new(object.to_sban, indices).pretty printer
      printer.flush
      return pp_io.to_s
    else
      return SBAN::Diagnostic.new(object.to_sban, indices).to_s
    end
  end

  def SBAN.hex(bytes : Bytes)
    return bytes.map(&.to_s(16, upcase: true).rjust(2, '0')).join(" ")
  end
end
