require "../engine/io"
require "colorize"

# A plaintext renderer primarily meant for testing purposes. It displays the badges passed by the engine directly to STDOUT with little processing.
module PlainRender
  class InterfaceError < Stelios::IO::Interface::InterfaceError
  end

  class Handler < Stelios::IO::Interface
    getter diagnostic : String = "Plaintext renderer"
    getter identifier : String = "Plaintext"

    struct Stelios::IO::Badge
      def instance_vars(handler, indent = 0)
        String.build do |io|
          {% for ivar in @type.instance_vars %}
            {% if ivar.id != "variant" %}
              io << "\n" << "\u251c".colorize(:dark_gray) << "{{ivar.id}}".colorize(:cyan) << " => "
              case val = @{{ivar.id}}
              when Badge then io << handler.convert_badge(val, indent)
              else io << val.to_s
              end
            {% end %}
          {% end %}
        end
      end
    end

    def initialize(@locale : Stelios::IO::Locale::Entry)
      spawn name: "input" do
        loop do
          i = gets.not_nil!.chomp
          break if i == "x"
          output(B::Text.new "Input event: #{i}")
        end
        exit(0)
      end
    end

    def convert_badge(badge : B, indent = 0)
      lines = ("\u2502" * indent).colorize(:dark_gray)
      s = String.build do |str|
        str << badge.class.to_s.gsub("Stelios::IO::Badge::", "B:").colorize(:yellow).bold
        case badge
        when B::Text
          str << "\n" << "\u251c".colorize(:dark_gray)
          case badge.key
          when Symbol
            str << badge.key.to_s.colorize(:cyan) << ":"
            locale_string = @locale.dig badge.key.to_s.split("__")
          else locale_string = badge.key.to_s
          end
          (locale_string.split LOCALE_PLACEHOLDER_FULL).each do |i|
            if LOCALE_PLACEHOLDER =~ i
              str << i.colorize(:light_cyan).underline
            else
              str << i
            end
          end
          badge.args.each_with_index do |b, i|
            str << "\n" << "\u251c".colorize(:dark_gray) << "%%#{i + 1} ".colorize(:cyan) << convert_badge(b, indent + 1)
          end
        else
          str << badge.instance_vars(self, indent + 1)
        end
        str << "\n" << "\u2514".colorize(:dark_gray) << badge.variant.reject { |_, v| v.nil? }.to_s.sub("{}".colorize(:red), &.== "{}").to_s
      end
      s.split("\n").join("\n#{lines}")
    end

    def output(badge : B)
      puts "#{elapsed.to_s.colorize(:light_blue)}\n#{convert_badge badge}"
      puts ""
    end

    def abort
      nil
    end
  end
end

# :nodoc:
class Object
  # :nodoc:
  def sub(s : T) forall T
    (yield self) ? s : self
  end
end
