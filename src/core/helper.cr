require "../engine/io"

module Stelios
  VERSION = Version::SHORT
  alias B = IO::Badge

  private def display_error(ex : Exception)
    puts "## #{ex.class.name} - #{ex.message} ##"
    puts ""
    puts "A fatal #{ex.class.name} occured.\nStelios couldn't recover from this error and was forced to close immediately.\nThis means that the application may not have exited cleanly"
    puts ""
    puts "Cause of error: #{ex.message}" if ex.message
    puts ""
    if ex.is_a? IO::Interface::InterfaceError
      puts "The error arose in the renderer, currently #{INF.identifier}"
      puts ""
    end

    unless ex.backtrace?.nil?
      puts "A backtrace is provided:"
      ex.backtrace.each do |frame|
        frame = frame.gsub(/^src\//, "")                  # Entry in Stelios code
        frame = frame.gsub(/.*\/crystal\/src\//, "(cr) ") # Entry in crystal code, usually not very relevant
        frame = frame.gsub(/\.cr(:\d+:\d+)/, "\\1")       # Reduce clutter
        frame = frame.gsub(/in '(.*)'$/, "[\\1]")         # Reduce clutter
        puts "  #{frame}"
      end
    else
      puts "No backtrace could be recovered"
    end
  end
end
