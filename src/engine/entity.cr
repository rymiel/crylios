require "../sban"
require "./stat"

module Stelios
  class Entity
    include SBAN::Serializable

    property name : String
    getter stat : StatUnion

    @[SBAN::Field(ignore: true)]
    @static_stat = RawStat.new
    @[SBAN::Field(ignore: true)]
    @mod_stat = StatHash.new

    def initialize(@name = "Unnamed entity")
      @stat = StatUnion.new(@static_stat, @mod_stat)
    end
  end
end