require "../entity.cr"
require "../stat.cr"

module Stelios
  class Entity::Player < Entity
    def initialize(@name = "Player")
      super
      @stat[Stat::Level] = 1_u16
    end
  end
end
