require "../render/terminal"
require "../render/plain"

module Stelios
  private def new_interface
    locale = File.open(File.expand_path "src/locale.yaml") do |file|
      IO::Locale::Entry.from_yaml(file)
    end

    {% if flag?(:plain) %}
    PlainRender::Handler.new(locale)
    {% else %}
    TerminalRender::Handler.new(locale)
    {% end %}
  end
end