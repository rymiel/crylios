require "./engine/**"
require "./core/**"
require "./render/helper"
require "./sban"
require "yaml"
require "log"

module Stelios
  extend self
  INF = new_interface

  Log.setup(:debug, Log::IOBackend.new File.new("debug.log", "a+"))
  fatal : Exception? = nil
  spawn name: "logic" do
    begin
      INF.output B::Text.new :test__welcome, INF.identifier, Version::LONG
      INF.output B::Text.new :test__renderer, INF.diagnostic

      emilia = Entity::Player.new("Emilia")
      emilia.stat[:dex] = 7_u16

      INF.output B::Text.new SBAN.diagnostic_for emilia, true, 65

      test2 = Inventory.new 10
      INF.output test2.add Item.new(name: "test", count: 2_u64, stack: 8_u64)
      INF.output test2.add Item.new(name: "bar", count: 3_u64, stack: 8_u64)
      INF.output test2.add Item.new(name: "test", count: 21_u64, stack: 8_u64)

      sleep # The game logic loop would go here
      # This fiber shouldn't close before everything else does
    rescue ex
      fatal = ex
    ensure
      INF.abort
      if fatal.nil?
        exit(0)
      else
        display_error fatal.not_nil!
        puts "Exiting with status 1..."
        exit(1)
      end
    end
  end
  # screen.sidebar = {"a", "b", "c"}

  # test2 = Inventory.new 10
  # screen.write(test2.add Item.new("test", 2_u64))
  # screen.write(test2.add Item.new("bar", 2_u64))
  # screen.write(test2.add Item.new("test", 21_u64))

  sleep
end
