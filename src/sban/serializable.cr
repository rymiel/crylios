require "./type"
require "log"

module SBAN
  annotation Field
  end

  # The `SBAN::Serializable` module automatically generates methods for SBAN serialization when included.
  #
  # ### Example
  #
  # ```
  # require "sban"
  #
  # class Location
  #   include SBAN::Serializable
  #
  #   @[SBAN::Field(key: "lat")]
  #   property latitude : Float64
  #
  #   @[SBAN::Field(key: "lng")]
  #   property longitude : Float64
  # end
  #
  # class House
  #   include SBAN::Serializable
  #
  #   property address : String
  #   property location : Location?
  # end
  #
  # house = House.from_sban({"address" => "Crystal Road 1234", "location" => {"lat" => 12.3, "lng" => 34.5}}.to_sban)
  # house.address  # => "Crystal Road 1234"
  # house.location # => #<Location:0x10cd93d80 @latitude=12.3, @longitude=34.5>
  # house.to_sban  # => Bytes[...]
  #
  # houses = Array(House).from_sban([{"address" => "Crystal Road 1234", "location" => {"lat" => 12.3, "lng" => 34.5}}].to_sban)
  # houses.size    # => 1
  # houses.to_sban # Bytes[...]
  # ```
  #
  # ### Usage
  #
  # Including `SBAN::Serializable` will create `#to_sban` and `self.from_sban` methods on the current class,
  # and a constructor which takes a `SBAN::Decoder`. By default, these methods serialize into a sban
  # object containing the value of every instance variable, the keys being the instance variable name.
  # Most primitives and collections supported as instance variable values (string, integer, array, hash, etc.),
  # along with objects which define to_sban and a constructor taking a `SBAN::Decoder`.
  # Union types are also supported, including unions with nil. If multiple types in a union parse correctly,
  # it is undefined which one will be chosen.
  #
  # To change how individual instance variables are parsed and serialized, the annotation `SBAN::Field`
  # can be placed on the instance variable. Annotating property, getter and setter macros is also allowed.
  # ```
  # require "sban"
  #
  # class A
  #   include SBAN::Serializable
  #
  #   @[SBAN::Field(key: "my_key")]
  #   getter a : Int32?
  # end
  # ```
  #
  # `SBAN::Field` properties:
  # * **ignore**: if `true` skip this field in serialization and deserialization (by default false)
  # * **key**: the value of the key in the json object (by default the name of the instance variable)
  # * **converter**: specify an alternate type for parsing and generation. The converter must define `from_sban(SBAN::Decoder)` and `to_sban(value, SBAN::Builder)` as class methods. Examples of converters are `Time::Format` and `Time::EpochConverter` for `Time`.
  # * **emit_null**: if `true`, emits a `null` value for nilable property (by default nulls are not emitted)
  # * **nil_as_undefined**: if `true`, when the value is `nil`, it is emitted as `undefined` (by default `nil` are encoded as `null`)
  #
  # Deserialization also respects default values of variables:
  # ```
  # require "sban"
  #
  # struct A
  #   include SBAN::Serializable
  #   @a : Int32
  #   @b : Float64 = 1.0
  # end
  #
  # A.from_sban({"a" => 1}.to_sban) # => A(@a=1, @b=1.0)
  # ```
  #
  # ### Extensions: `SBAN::Serializable::Unmapped`.
  #
  # If the `SBAN::Serializable::Unmapped` module is included, unknown properties in the SBAN
  # document will be stored in a `Hash(String, SBAN::Type)`. On serialization, any keys inside sban_unmapped
  # will be serialized and appended to the current json object.
  # ```
  # require "sban"
  #
  # struct A
  #   include JSON::Serializable
  #   include JSON::Serializable::Unmapped
  #   @a : Int32
  # end
  #
  # a = A.from_json(%({"a":1,"b":2})) # => A(@json_unmapped={"b" => 2_i64}, @a=1)
  # a.to_json                         # => {"a":1,"b":2}
  # ```
  #
  #
  # ### Class annotation `SBAN::Serializable::Options`
  #
  # supported properties:
  # * **emit_nulls**: if `true`, emits a `null` value for all nilable properties (by default nulls are not emitted)
  # * **nil_as_undefined**: if `true`, emits a `nil` value as undefined (by default nil emits `null`)
  #
  # ```
  # require "json"
  #
  # @[SBAN::Serializable::Options(emit_nulls: true)]
  # class A
  #   include JSON::Serializable
  #   @a : Int32?
  # end
  # ```
  #
  # ### Discriminator field
  #
  # A very common JSON serialization strategy for handling different objects
  # under a same hierarchy is to use a discriminator field. For example in
  # [GeoJSON](https://tools.ietf.org/html/rfc7946) each object has a "type"
  # field, and the rest of the fields, and their meaning, depend on its value.
  #
  # You can use `JSON::Serializable.use_json_discriminator` for this use case.
  module Serializable
    annotation Options
    end

    macro included
      # Define a `new` directly in the included type,
      # so it overloads well with other possible initializes

      def self.new(decoder : ::SBAN::Decoder)
        new_from_sban_decoder(decoder)
      end

      private def self.new_from_sban_decoder(decoder : ::SBAN::Decoder)
        instance = allocate
        instance.initialize(__decoder_for_sban_serializable: decoder)
        GC.add_finalizer(instance) if instance.responds_to?(:finalize)
        instance
      end

      extend SBAN::AutoFieldIndices
      # When the type is inherited, carry over the `new`
      # so it can compete with other possible initializes

      macro inherited
        def self.new(decoder : ::SBAN::Decoder)
          new_from_sban_decoder(decoder)
        end
      end
    end

    def initialize(*, __decoder_for_sban_serializable decoder : ::SBAN::Decoder)
      {% begin %}
        {% properties = {} of Nil => Nil %}
        {% count = -1 %}
        {% for ivar in @type.instance_vars %}
          {% ann = ivar.annotation(::SBAN::Field) %}
          {% unless ann && ann[:ignore] %}
            {% count = (ann && ann[:index]) || count + 1 %}
            {%
              properties[ivar.id] = {
                type:        ivar.type,
                index:       count,
                has_default: ivar.has_default_value?,
                default:     ivar.default_value,
                nilable:     ivar.type.nilable?,
                converter:   ann && ann[:converter],
              }
            %}
          {% end %}
        {% end %}

        {% for name, value in properties %}
          %var{name} = nil
          %found{name} = false
        {% end %}

        begin
          decoder.read_begin_hash
        rescue exc : ::SBAN::ParseError
          raise ::SBAN::SerializationError.new(exc.message, self.class.to_s, nil)
        end

        decoder.consume_hash do
          key = decoder.read_int
          case key
          {% for name, value in properties %}
            when {{value[:index]}}
              %found{name} = true
              begin
                %var{name} =
                  {% if value[:nilable] || value[:has_default] %} decoder.read_nil_or { {% end %}

                  {% if value[:converter] %}
                    {{value[:converter]}}.from_sban(decoder)
                  {% else %}
                    ::Union({{value[:type]}}).new(decoder)
                  {% end %}

                {% if value[:nilable] || value[:has_default] %} } {% end %}
              rescue exc : ::SBAN::ParseError
                raise ::SBAN::SerializationError.new(exc.message, self.class.to_s, {{value[:index]}}.to_s)
              end
          {% end %}
          else
            on_unknown_sban_attribute(decoder, key)
          end
        end

        {% for name, value in properties %}
          {% unless value[:nilable] || value[:has_default] %}
            if %var{name}.nil? && !%found{name} && !::Union({{value[:type]}}).nilable?
              raise ::SBAN::SerializationError.new("Missing SBAN attribute: {{value[:index].id}}", self.class.to_s, nil)
            end
          {% end %}

          {% if value[:nilable] %}
            {% if value[:has_default] != nil %}
              @{{name}} = %found{name} ? %var{name} : {{value[:default]}}
            {% else %}
              @{{name}} = %var{name}
            {% end %}
          {% elsif value[:has_default] %}
            @{{name}} = %var{name}.nil? ? {{value[:default]}} : %var{name}
          {% else %}
            @{{name}} = (%var{name}).as({{value[:type]}})
          {% end %}

        {% end %}
      {% end %}
      after_initialize
    end

    protected def after_initialize
    end

    protected def on_unknown_sban_attribute(decoder, key)
      raise ::SBAN::SerializationError.new("Unknown SBAN attribute: #{key}", self.class.to_s, nil)
    end

    protected def on_to_sban(sban : ::SBAN::Encoder)
    end

    def to_sban(sban : ::SBAN::Encoder)
      {% begin %}
        {% options = @type.annotation(::SBAN::Serializable::Options) %}
        {% emit_nulls = options && options[:emit_nulls] %}
        {% nil_as_undefined = options && options[:nil_as_undefined] %}

        {% properties = {} of Nil => Nil %}
        {% count = -1 %}
        {% for ivar in @type.instance_vars %}
          {% ann = ivar.annotation(::SBAN::Field) %}
          {% count = (ann && ann[:index]) || count + 1 %}
          {% unless ann && ann[:ignore] %}
            {%
              properties[ivar.id] = {
                type:             ivar.type,
                index:              count,
                converter:        ann && ann[:converter],
              }
            %}
          {% end %}
        {% end %}

        sban.write_object_start({{ properties.size }})
        {% for name, value in properties %}
          _{{name}} = @{{name}}

          # Write the key of the map
          sban.write({{value[:index]}})

          {% if value[:converter] %}
            if _{{name}}
              {{ value[:converter] }}.to_sban(_{{name}}, sban)
            else
              sban.write(nil)
            end
          {% else %}
            _{{name}}.to_sban(sban)
          {% end %}

        {% end %}
        on_to_sban(sban)
      {% end %}
    end

    module Unmapped
      @[SBAN::Field(ignore: true)]
      property sban_unmapped : Hash(String, ::SBAN::Type) = Hash(String, ::SBAN::Type).new

      protected def on_unknown_sban_attribute(decoder, key)
        sban_unmapped[key] = begin
          decoder.read_value
        rescue exc : ::SBAN::ParseError
          raise ::SBAN::SerializationError.new(exc.message, self.class.to_s, key)
        end
      end

      protected def on_to_sban(sban : ::SBAN::Encoder)
        sban_unmapped.each do |key, value|
          sban.write(key)
          value.to_sban(sban)
        end
      end
    end

    # Tells this class to decode SBAN by using a field as a discriminator.
    #
    # - *field* must be the field name to use as a discriminator
    # - *mapping* must be a hash or named tuple where each key-value pair
    #   maps a discriminator value to a class to deserialize
    #
    # For example:
    #
    # ```
    # require "sban"
    #
    # abstract class Shape
    #   include SBAN::Serializable
    #
    #   use_sban_discriminator "type", {point: Point, circle: Circle}
    #
    #   property type : String
    # end
    #
    # class Point < Shape
    #   property x : Int32
    #   property y : Int32
    # end
    #
    # class Circle < Shape
    #   property x : Int32
    #   property y : Int32
    #   property radius : Int32
    # end
    #
    # TODO: Update examples
    # Shape.from_sban(%({"type": "point", "x": 1, "y": 2}))               # => #<Point:0x10373ae20 @type="point", @x=1, @y=2>
    # Shape.from_sban(%({"type": "circle", "x": 1, "y": 2, "radius": 3})) # => #<Circle:0x106a4cea0 @type="circle", @x=1, @y=2, @radius=3>
    # ```
    # macro use_sban_discriminator(field, mapping)
    #   {% unless mapping.is_a?(HashLiteral) || mapping.is_a?(NamedTupleLiteral) %}
    #     {% mapping.raise "mapping argument must be a HashLiteral or a NamedTupleLiteral, not #{mapping.class_name.id}" %}
    #   {% end %}

    #   def self.new(decoder : ::SBAN::Decoder)
    #     discriminator_value = nil

    #     # Try to find the discriminator while also getting the raw
    #     # string value of the parsed JSON, so then we can pass it
    #     # to the final type.
    #     json = String.build do |io|
    #       JSON.build(io) do |builder|
    #         builder.start_object
    #         pull.read_object do |key|
    #           if key == {{field.id.stringify}}
    #             discriminator_value = pull.read_string
    #             builder.field(key, discriminator_value)
    #           else
    #             builder.field(key) { pull.read_raw(builder) }
    #           end
    #         end
    #         builder.end_object
    #       end
    #     end

    #     unless discriminator_value
    #       raise ::JSON::MappingError.new("Missing JSON discriminator field '{{field.id}}'", to_s, nil, *location, nil)
    #     end

    #     case discriminator_value
    #     {% for key, value in mapping %}
    #       when {{key.id.stringify}}
    #         {{value.id}}.from_json(json)
    #     {% end %}
    #     else
    #       raise ::JSON::MappingError.new("Unknown '{{field.id}}' discriminator value: #{discriminator_value.inspect}", to_s, nil, *location, nil)
    #     end
    #   end
    # end
  end

  alias FieldIndexTuple = {name: String}

  module FieldIndices
    abstract def sban_field_indices : Hash(Int32, FieldIndexTuple)
  end

  module AutoFieldIndices
    include FieldIndices
    def sban_field_indices : Hash(Int32, FieldIndexTuple)
      {% begin %}
        {% properties = {} of Nil => Nil %}
        {% count = -1 %}
        {% for ivar in @type.instance_vars %}
          {% ann = ivar.annotation(::SBAN::Field) %}
          {% unless ann && ann[:ignore] %}
            {% count = (ann && ann[:index]) || count + 1 %}
            {% properties[count] = {name: ivar.stringify} %}
          {% end %}
        {% end %}
        {{ properties }} of Int32 => FieldIndexTuple
      {% end %}
    end
  end
end
