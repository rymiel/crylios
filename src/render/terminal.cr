require "./terminal/terminal-input"
require "../engine/io"
require "yaml"

# A TUI-like renderer. Uses only text in a terminal, but provides more sophisticated layout and styling options.
module TerminalRender
  CONSOLE = Console.new

  class InterfaceError < Stelios::IO::Interface::InterfaceError
  end

  module ANSI
    RESET_COLOR  = "\e[m"
    RESET_CURSOR = "\e[H"
    SET_TITLE    = ->(title : String) { "\e]0;#{title}\a" }
    SET_COLOR    = ->(color : Int32) { "\e[#{color}m" }

    COLOR_ORDER  = ["f", "F", "b", "B", "S", "X"]
    COLOR_LOOKUP = {
      "X" => [30, 90, 40, 100, 0, 0],
      "R" => [31, 91, 41, 101, 0, 0],
      "G" => [32, 92, 42, 102, 0, 0],
      "Y" => [33, 93, 43, 103, 0, 0],
      "B" => [34, 94, 44, 104, 1, 0],
      "P" => [35, 95, 45, 105, 5, 0],
      "C" => [36, 96, 46, 106, 0, 0],
      "W" => [37, 97, 47, 107, 0, 0],
      "U" => [0, 0, 0, 0, 4, 0],
      "E" => [0, 0, 0, 0, 22, 0],
      "I" => [0, 0, 0, 0, 7, 0],
      "F" => [0, 0, 0, 0, 2, 0],
    }

    ERASE_ALL = "\e[2J"
  end

  {% begin %}
  COLOR_CODE = /%([{{ ANSI::COLOR_ORDER.join("").id }}])([{{ ANSI::COLOR_LOOKUP.keys.join("").id }}])/
  {% end %}

  module Color
    extend self

    def size(text : String)
      text.gsub(COLOR_CODE, "").size
    end

    def rjust(text : String, width : Int)
      text.rjust(width + text.size - size(text))
    end

    def ljust(text : String, width : Int)
      text.ljust(width + text.size - size(text))
    end

    def center(text : String, width : Int)
      size = size(text)
      width += text.size - size
      padding = (width - size) // 2
      text.ljust(size + padding).rjust(width)
    end

    def wrap(text : String, width : Int)
      count = 0
      skip = 0_u8
      result = String.build(text.size) do |s|
        text.each_char_with_index do |c, i|
          s << c
          if skip > 0
            skip -= 1
            next
          end
          if COLOR_CODE === text[i..]
            skip = 2_u8
          else
            count += 1
            if count == width
              count = 0
              s << "\n" if i != text.size - 1
            end
          end
        end
      end
      result.split("\n")
    end

    def lookup(type : String, color : String)
      ANSI::COLOR_LOOKUP[color][ANSI::COLOR_ORDER.index(type).not_nil!]
    end

    def color(text : String)
      text.gsub(COLOR_CODE) { |s| ANSI::SET_COLOR.call lookup(s[1].to_s, s[2].to_s) }
    end
  end

  class Console
    # Returns the width of the console screen.
    getter width : UInt16 = `tput cols`.to_u16

    # Returns the height of the console screen.
    getter height : UInt16 = `tput lines`.to_u16

    @convert : Proc(String, String) = ->Color.color(String)

    # Updates the values of `width` and `height`
    def update
      @width = `tput cols`.to_u16
      @height = `tput lines`.to_u16
    end

    # Sets the title of the window to *title*.
    def title=(title)
      print ANSI::SET_TITLE.call(title)
    end

    def write(text)
      print @convert.call text
    end

    # Disables or enables the alternative screen buffer for the console which doesn't have a scrollbar.
    def scrollbar=(scrollbar : Bool)
      system(scrollbar ? "tput rmcup" : "tput smcup")
    end

    # Moves the cursor up by *cells*.
    def move_up(cells = 1)
      print "\e[#{cells}A"
    end

    # Moves the cursor down by *cells*.
    def move_down(cells = 1)
      print "\e[#{cells}B"
    end

    # Moves the cursor right by *cells*.
    def move_right(cells = 1)
      print "\e[#{cells}C"
    end

    # Moves the cursor left by *cells*.
    def move_left(cells = 1)
      print "\e[#{cells}D"
    end

    # Shows or hides the cursor.
    def visible=(visible : Bool)
      print(visible ? "\e[?25h" : "\e[?25l")
    end

    # Sets the position of the cursor to *position*.
    def position=(position : Tuple(UInt16, UInt16))
      # Row and column for the ANSI escape sequence need to be one-based
      print "\e[#{position[1] + 1};#{position[0] + 1}H"
    end

    def reset_color
      print ANSI::RESET_COLOR
    end

    def clear!
      print ANSI::ERASE_ALL
      print ANSI::RESET_CURSOR
    end
  end

  class CanvasLine
    setter content : String
    property line : UInt16
    property width : UInt16
    property prefix : String
    property suffix : String
    property rendered : Bool = false

    def combined
      prefix + @content + suffix
    end

    def content
      combined + (" " * (width - size))
    end

    def raw_content
      @content
    end

    def raw_content=(text)
      @content = text
    end

    def size
      Color.size(@content)
    end

    def initialize(@content, @line, @width, @prefix = "", @suffix = "")
    end
  end

  class Canvas
    getter width : UInt16
    getter height : UInt16
    getter x : UInt16
    getter y : UInt16
    getter content : Array(CanvasLine) = Array(CanvasLine).new
    property atY : UInt16 = 0_u16

    private def absY
      y + @atY
    end

    def initialize(@width, @height, @x, @y)
      clear!
    end

    def clear!
      @content = Array(CanvasLine).new(height) { |i| CanvasLine.new("", i.to_u16, width) }
    end

    def write(text : String)
      content[@atY] = CanvasLine.new(text, @atY, width) if @atY < height
      @atY += 1
    end

    def render
      content.reject(&.rendered).each do |text|
        CONSOLE.position = {x, y + text.line}
        CONSOLE.write(text.content)
        CONSOLE.reset_color
        text.rendered = true
      end
    end

    def derender
      content.each &.rendered = false
    end
  end

  class Sidebar < Canvas
    getter options : Indexable(String) = [] of String
    getter focus : Int32 = 0
    getter prev_overflow : Int32?

    def focus=(new_focus : Int32)
      index = new_focus
      if index >= @options.size
        index = 0
      elsif index < 0
        index = @options.size - 1
      end
      set_view index
      @focus = index
    end

    def options=(strings : Indexable(String))
      @options = strings
      self.focus = 0
      @prev_overflow = nil
    end

    def content
      @content.each do |line|
        if line.prefix != Screen.accent_color
          line.rendered = false
          line.prefix = Screen.accent_color
        end
      end
      if !options.empty?
        overflow = prev_overflow || 0
        overflow = @content[focus - overflow]
        overflow.prefix = Screen.focus_accent_color
        overflow.rendered = false
      end
      @content
    end

    def set_view(view_focus : Int32 = 0)
      overflow = (view_focus - (height // 2)).clamp(0, Math.max(0, @options.size - height))
      if overflow != prev_overflow
        option_view = @options.to_a.map_with_index { |j, i| j if (overflow..height).includes?(i) }
        option_view.compact!
        @content = Array(CanvasLine).new(height) do |i|
          option = option_view[i]? || ""
          Color.rjust(option, width)
          CanvasLine.new(option, i.to_u16, width)
        end
      end
      @prev_overflow = overflow
    end

    def clear!
      self.options = [] of String
    end
  end

  class Screen
    class_getter accent_color = "%bB"
    class_getter focus_accent_color : String = @@accent_color.upcase
    class_getter page_width : UInt16 = CONSOLE.width - @@sidebar_width
    class_getter page_height : UInt16 = CONSOLE.height - @@header - @@footer
    @@sidebar_width : UInt16 = ((CONSOLE.width.to_i32 - 80) / 8 + 8).clamp(7, 24).to_u16
    @@header = 1_u16
    @@footer = 5_u16
    @@max_pages = 10_u8

    @current_page : UInt8 = 0_u8
    getter alive = true

    getter pages : Deque(Canvas) = Deque(Canvas).new(@@max_pages)
    getter sidebar : Sidebar = Sidebar.new(@@sidebar_width, @@page_height, @@page_width, @@header)
    getter extra_line : String?

    def initialize(title : String)
      CONSOLE.reset_color
      CONSOLE.scrollbar = false
      CONSOLE.visible = false
      CONSOLE.title = title
      CONSOLE.clear!
      CONSOLE.write "#{@@accent_color}%FW%SB#{Color.center(title, CONSOLE.width)}%XE\n"
      @sidebar = Sidebar.new(@@sidebar_width, @@page_height, @@page_width, @@header)
      add_page
      @sidebar.render
    end

    def exit!
      CONSOLE.scrollbar = true
      CONSOLE.visible = true
      CONSOLE.title = ""
      CONSOLE.reset_color
      CONSOLE.clear!
      @alive = false
    end

    def last
      @pages.last
    end

    def last?
      @current_page == @pages.size - 1
    end

    def last=(new_page : Canvas)
      @pages.shift if @pages.size == @@max_pages
      @pages.push(new_page)
    end

    def page
      @pages[@current_page]
    end

    def extra_line=(text : String?)
      @extra_line = text
      render_extra_line(text) if last?
    end

    def render_extra_line
      render_extra_line nil
    end

    def render_extra_line(text : String?, focus : Bool = false)
      text ||= ""
      color = focus ? Screen.focus_accent_color : Screen.accent_color
      text = Color.ljust("#{color} %SB%FW* #{text}", CONSOLE.width) + "%XE"
      CONSOLE.position = {0_u16, CONSOLE.height - @@footer}
      CONSOLE.write text
    end

    def add_page(render render_page : Bool = false)
      self.last = Canvas.new(Screen.page_width, Screen.page_height - 1, 0_u16, @@header + 1)
      move_focus(0, render: render_page)
    end

    def write(text : String, newlines : Int32 = 0, nonbreaking : Bool = false)
      split_text = text.split "\n"
      new_text = [] of String
      split_text.each { |i| new_text.concat Color.wrap(i, Screen.page_width) }
      add_page if nonbreaking && last.atY + new_text.size >= Screen.page_height - 1
      new_text.each do |i|
        add_page if last.atY >= Screen.page_height - 1
        last.write i
      end
      page.render
      last.atY += newlines
    end

    def set_focus(new_focus : UInt8, render render_page : Bool = true)
      if render_page
        page.derender
        @pages[new_focus].render
      end
      @current_page = new_focus
      if !last?
        render_extra_line "Continued"
      else
        render_extra_line @extra_line
      end
    end

    def move_focus(difference : Int, render render_page : Bool = true)
      set_focus((@current_page + difference).clamp(0_u8, @pages.size - 1).to_u8, render: render_page)
    end

    def sidebar=(strings : Indexable(String))
      @sidebar.options = strings
      @sidebar.render if last?
    end
  end

  class Handler < Stelios::IO::Interface
    getter diagnostic : String = "Console: #{CONSOLE.width} x #{CONSOLE.height}, Page: #{Screen.page_width} x #{Screen.page_height}"
    getter identifier : String = "Terminal"
    LOCALE_PLACEHOLDER  = /%%(\d)/
    VARIANT_PLACEHOLDER = /%!(\d)\{((?:(?:\w+ \w+\|)*(?:\w+ \w+))?)\}/
    @@terminate = Channel(Bool).new
    @@dead = false

    def initialize(@locale : Stelios::IO::Locale::Entry)
      @screen = Screen.new "Stelios Terminal #{Stelios::Version::SHORT}"
      @input = Input.new

      spawn name: "terminal input" do
        loop do
          select
          when should_terminate = @@terminate.receive
            @@dead = true
            break if should_terminate
          else
            input = @input.read_keypress(true)
            unless input.nil?
              @screen.write (input.each_byte.map &.to_s 16).join " "
            end
          end
        rescue e : Input::Interrupt
          @@dead = true
          abort
          exit(0)
        end
      end
    end

    private def convert_badge(badge : B)
      case badge
      when B::Text
        resolved =
          case badge.key
          when Symbol then @locale.dig badge.key.to_s.split("__")
          else             badge.key.to_s
          end

        resolved = resolved.gsub(LOCALE_PLACEHOLDER) do
          sub = badge.args[$~[1].to_i - 1]? || raise InterfaceError.new "Locale key #{badge.key} tried to replace placeholder #{$~[0]}, but #{badge.args.size} substitutions were given"
          convert_badge(sub)
        end

        resolved = resolved.gsub(VARIANT_PLACEHOLDER) do
          sub = badge.args[$~[1].to_i - 1]? || raise InterfaceError.new "Locale key #{badge.key} tried to replace placeholder #{$~[0]}, but #{badge.args.size} substitutions were given"
          variants = $~[2].split '|'
          variants.each do |i|
            key, val = i.split ' '
            sub.variant[key] = val
          end
          convert_badge(sub)
        end

        resolved =
          case v = badge.variant["emphasis"]
          when "highlight"      then "%FY#{resolved}%XE"
          when "highlight_good" then "%FG#{resolved}%XE"
          when nil              then resolved
          else                       raise InterfaceError.new "Unknown emphasis #{v}"
          end

        resolved
      when B::ContainerAddResult
        badge.to_s
      else
        raise InterfaceError.new "Can't display badge of type #{badge.class}"
      end
    end

    def output(badge : B)
      @screen.write convert_badge badge
    end

    def abort
      @screen.exit!
      @@terminate.send(true) unless @@dead
      nil
    end
  end
end
