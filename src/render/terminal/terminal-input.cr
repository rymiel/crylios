require "./key_event"
require "./keys"

module TerminalRender
  class Input
    # Raised when a user hits ctrl-c
    class Interrupt < Exception; end

    class Console
      ESC = '\e'
      CSI = "\e["

      TIMEOUT = 100.milliseconds

      getter keys : Hash(String, String)
      getter escape_codes : Tuple(Array(UInt8), Array(UInt8))

      protected getter input : IO::FileDescriptor

      def initialize(@input : IO::FileDescriptor)
        @keys = CTRL_KEYS.merge(KEYS)
        @escape_codes = {[ESC.ord.to_u8], CSI.bytes}
      end

      def get_char(nonblock : Bool = false)
        ret = nil
        @input.raw do
          @input.noecho do
            if nonblock
              @input.wait_readable(TIMEOUT)
              ret = @input.read_char
            else
              ret = @input.read_char
            end
          end
        end
        ret ? ret.not_nil! : nil
      rescue
        nil
      end
    end

    # Key codes
    CARRIAGE_RETURN = 13
    NEWLINE         = 10
    BACKSPACE       =  8
    DELETE          = 27

    VERSION = "0.1.0"

    getter input : IO::FileDescriptor
    getter output : IO::FileDescriptor
    getter env : Hash(String, String)

    getter console : Console

    def initialize(@input : IO::FileDescriptor = STDIN,
                   @output : IO::FileDescriptor = STDOUT,
                   @env : Hash(String, String) = ENV.to_h)
      @console = Console.new(@input)
    end

    # Get input in unbuffered mode.
    def unbuffered(&block)
      buffering = @output.sync?
      # Immidiately flush output
      @output.sync = true
      yield
    ensure
      @output.sync = buffering
    end

    # Reads a keypress, including invisible multibyte codes
    # and return a character as a String.
    #
    # Nothing is echoed to the console.
    def read_keypress(nonblock = false)
      codes = unbuffered { get_codes(nonblock: nonblock) }
      codes ? codes.map(&.chr).join : nil
    end

    # Get input code points.
    def get_codes(codes = [] of Int32, nonblock = false)
      char = console.get_char(nonblock: nonblock)
      raise Interrupt.new if console.keys[char.to_s]? == "ctrl_c"
      return if char.nil?
      codes << char.ord

      condition = ->(escape : Array(UInt8)) do
        (codes - escape).empty? ||
        (escape - codes).empty? &&
        !(64..126).covers?(codes.last)
      end

      while console.escape_codes.any?(condition)
        char_codes = get_codes(codes: codes, nonblock: true)
        break if char_codes.nil?
      end

      codes
    end
  end
end
