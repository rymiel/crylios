require "./item"

module Stelios::IO
  module Locale
    class Entry
      property value : String?
      @entries : Hash(String, Entry)?

      def [](key : String)
        entries = @entries ||= {} of String => Entry
        entries[key] ||= Entry.new
      end

      def []=(key : String, value : String)
        self[key].value = value
      end

      def initialize
      end

      def initialize(ctx : YAML::ParseContext, node : YAML::Nodes::Node)
        case parsed = Union(String, Hash(String, Entry)).new(ctx, node)
        when String then @value = parsed
        when Hash   then @entries = parsed
        end
      end

      def dig(keys : Array(String))
        dig?(keys).value || raise KeyError.new("No such locale key: #{keys.join("__")}")
      end

      def dig?(keys : Array(String))
        key = keys.first
        if (value = self[key]) && value.responds_to?(:dig?)
          if keys.size > 2
            value.dig?(keys[1..])
          else
            value.dig?(keys[1])
          end
        else
          raise KeyError.new("Hash value not diggable for key: #{key.inspect}")
        end
      end

      def dig?(key : String)
        self[key]
      end
    end
  end

  # The output format of the Stelios engine.
  # For every kind of output, the engine outputs a `Badge`, which defines its own fields. A badge shouldn't contain any complex
  # objects, though it can recursively contain other badges.
  #
  # For example, instead of passing a `Stelios::Item` class to the renderer, it should instead output a `Stelios::IO::Badge::Item` badge,
  # containing the same information as the class, but statically. A renderer is then free to parse this badge however necessary, for example,
  # a graphical renderer might use an icon from the item instead of the item name.
  #
  # Additionally, badges can contain a `#variant` hash that contains more dynamic metadata about the badge. See `#variant` for more.
  abstract struct Badge
    # Badges can contain a hash that contains more dynamic metadata about the badge.
    #
    # This variant information can be set by the engine, but also, for example, the locale system.
    #
    # For example, a locale string can set the plurality of an item name, of for languages with complex case system, the case of the item name
    # within the sentence. Handling such information is left to the renderer.
    # Descendants of `Badge` should override the default empty hash if they contain any additional information.
    getter variant : Hash(String, String?) = {} of String => String?

    # The most basic `Badge` variant, for displaying raw text to the renderer that doesn't have any additional info attached to it.
    # Despite being a basic badge format, it isn't a simple one. `Text` badges are used for translation keys in localization.
    # The `#key` of a `Text` badge may be a string, which means an unlocalized string, or a symbol, which means a locale lookup. The locale
    # lookup is left to the renderer. In either case, the resulting string may additionally contain localization placeholders.
    # These placeholders are replaced by the renderer with other `Badge`s from the `#args` array.
    #
    # There are 3 kinds of placeholders available:
    #
    # `%%n`. This is a simple substitution, where the placeholder is replaced by the badge at the nth index in
    # the `#args` array.
    #
    # `%!n{key val}`. Similar to the above, but applies variant information. The key and value pairs shouldn't contain spaces. To specify multiple
    # key-value pairs, separate them with `|`, for example, `%!1{key1 val1|key2 val2|key3 val3}`. See `Badge#variant` for more about variants.
    #
    # `%?n{k string}`. Similar to the above, but specifically for pluralization. This means the renderer picks the correct plural variant according
    # to the placeholder key. Also similarly, multiple plural forms are separated with `|`, however the strings here *may* contain spaces. k stands
    # for a single character specifying the kind of the branch. An English localization might use `1` for singular and `+` for plural, but is
    # arbitrary, with only the limiting factor being that it must be a single character. Example: `He was offering $?1{1 a single gold coin|+ %%1 gold coins}`
    struct Text < Badge
      getter key : String | Symbol
      getter args : Array(Badge)

      def initialize(@key, @args : Array(Badge) = [] of Badge, *, emphasis : String | Symbol | Nil = nil)
        @variant = {"emphasis" => emphasis.try &.to_s} of String => String?
      end

      def initialize(@key, *args : Badge | String, emphasis : String | Symbol | Nil = nil)
        @args = args.to_a.map { |b|
          case b
          in Badge  then b.as Badge
          in String then Text.new b
          end
        }
        @variant = {"emphasis" => emphasis.try &.to_s} of String => String?
      end
    end

    struct Item < Badge
      @name : String

      def initialize(@name)
      end

      def initialize(from_item : Stelios::Item)
        @name = from_item.name
      end
    end

    struct ContainerAddResult < Badge
      getter original : Item
      getter entries : Array(AddEntry)
      getter terminate : TerminationReason

      alias AddEntry = {slot: UInt64, delta_count: UInt64, result_count: UInt64}

      enum TerminationReason
        None   # successful item addition
        NoRoom # ran out of space midway
        Full   # couldn't add any to begin with
      end

      def initialize(@original, @entries, @terminate)
      end
    end
  end

  # The abstract base class where renderers should derive from.
  abstract class Interface
    alias B = Badge
    LOCALE_PLACEHOLDER      = /%%(\d)/
    LOCALE_PLACEHOLDER_FULL = /(%%\d)/
    VARIANT_PLACEHOLDER     = /%!(\d)\{((?:(?:\w+ \w+\|)*(?:\w+ \w+))?)\}/
    @start = Time.monotonic

    # Creates the renderer and passes the locale context to it. Ran when the game is run.
    abstract def initialize(locale : Locale::Entry)
    # Provides a `Badge` for the renderer to output to the user. What do actually do with it is up to the renderer.
    abstract def output(badge : Badge)
    # Provide a short identifing string about the type of renderer being run.
    abstract def identifier : String
    # Additional expanded debug info, if relevant
    abstract def diagnostic : String
    # Run when the game runs into an unhandled exception. The renderer should unload everything here.
    abstract def abort

    # Microseconds since the renderer was created.
    def elapsed
      (Time.monotonic - @start).total_microseconds
    end

    class InterfaceError < Exception
    end
  end
end
