# crylios - Crystal Stelios

An RPG written in Crystal, called "Stelios". Still *very* early.

While this originally started out as an experimental text-based rpg written in python, it is now a much more generalized and complicated project.

This version of Stelios uses a more abstract system where the **engine** handling the game and the **renderer** handling the user output and input are almost completely separate. This allows the game to potentially use multiple different "front-ends" for the same Stelios "back-end" that is the engine.
