require "./lexer"
require "./serializable"
require "log"

# Reads a SBAN input into a diagnostic string.
# This consumes the IO and is mostly useful to tests again the example
# provided in the RFC and ensuring a correct functioning of the `SBAN::Lexer`.
class SBAN::Diagnostic
  @lexer : Lexer
  @context : Hash(Int32, SBAN::FieldIndexTuple)?

  def initialize(input, @context = nil)
    @lexer = Lexer.new(input)
  end

  def self.to_s(bytes : Bytes) : String
    self.new(bytes).to_s
  end

  # Reads the content of the IO and prints out a diagnostic string
  # represation of the input.
  def to_s : String
    result = ""
    while value = next_value
      result += value
    end
    result
  end

  private def next_value : String?
    token = @lexer.next_token
    return nil unless token
    to_diagnostic(token)
  end

  def pretty(pp)
    while next_value_pretty pp
    end
  end

  private def next_value_pretty(pp) : Bool
    token = @lexer.next_token
    return false unless token
    to_diagnostic_pretty(token, pp)
    true
  end

  private def to_diagnostic(token : Token::T) : String
    case token
    when Token::IntT
      token.value.to_s
    when Token::StringT
      if token.chunks
        chunks = chunks(token.value, token.chunks.not_nil!)
        "(_ #{chunks.map { |s| string(s) }.join(", ")})"
      else
        string(token.value)
      end
    when Token::BytesT
      if token.chunks
        chunks = chunks(token.value, token.chunks.not_nil!)
        "(_ #{chunks.map { |b| bytes(b) }.join(", ")})"
      else
        bytes(token.value)
      end
    when Token::ArrayT
      arr = read_array(token.size)
      return "[#{arr.join(", ")}]" if token.size
      "[_ #{arr.join(", ")}]"
    when Token::MapT
      hash_body, is_class = read_hash(token.size)
      return "<#{hash_body}>" if is_class && token.size
      return "{#{hash_body}}" if token.size
      "{_ #{hash_body}}"
    when Token::SimpleValueT
      token.value.to_diagnostic
    when Token::FloatT
      return "NaN" if token.value.nan?
      return token.value.to_s if token.value.finite?

      case value = token.value
      when Float32
        return "Infinity" if value == Float32::INFINITY
        "-Infinity"
      when Float64
        return "Infinity" if value == Float64::INFINITY
        "-Infinity"
      else
        token.value.to_s
      end
    else
      token.inspect
    end
  end

  private def to_diagnostic_pretty(token : Token::T, pp) : Nil
    case token
    when Token::IntT
      pp.text token.value.to_s
    when Token::StringT
      if token.chunks
        chunks = chunks(token.value, token.chunks.not_nil!)
        pp.list("(_", chunks, ")") { |s| string(s) }
      else
        pp.text string(token.value)
      end
    when Token::BytesT
      if token.chunks
        chunks = chunks(token.value, token.chunks.not_nil!)
        "(_ #{chunks.map { |b| bytes(b) }.join(", ")})"
      else
        bytes(token.value)
      end
    when Token::ArrayT
      arr = read_array(token.size)
      return "[#{arr.join(", ")}]" if token.size
      "[_ #{arr.join(", ")}]"
    when Token::MapT
      is_class = !@context.nil?
      l_delim = is_class ? "<" : "{"
      r_delim = is_class ? ">" : "}"
      pp.surround(l_delim, r_delim) do
        read_hash(token.size, pp)
      end
    when Token::SimpleValueT
      token.value.to_diagnostic
    when Token::FloatT
      return "NaN" if token.value.nan?
      return token.value.to_s if token.value.finite?

      case value = token.value
      when Float32
        return "Infinity" if value == Float32::INFINITY
        "-Infinity"
      when Float64
        return "Infinity" if value == Float64::INFINITY
        "-Infinity"
      else
        token.value.to_s
      end
    else
      token.inspect
    end
  end

  private def read_array(size : Int32?) : Array(String)
    arr = size ? Array(String).new(size) : Array(String).new

    consume_array_body(size) do |token|
      arr << to_diagnostic(token)
    end

    arr
  end

  # Reads the hash, returning an array of key-pairs strings already
  # correctly formatted in the diagnostic notation. Also returns a bool if the map
  # was parsed as an sban class instead.
  private def read_hash(size : Int32?) : Tuple(String, Bool)
    key_pairs = Array(String).new
    context_save = @context
    @context = nil
    consume_map_body(size) { |pairs|
      if context_save.nil?
        key_pairs << "#{to_diagnostic(pairs[0])}: #{to_diagnostic(pairs[1])}"
      else
        field_name = context_save.not_nil![pairs[0].as(Token::IntT).value]
        key_pairs << "`#{field_name["name"]}`#{to_diagnostic(pairs[0])}: #{to_diagnostic(pairs[1])}"
      end
    }
    {key_pairs.join(", "), !context_save.nil?}
  end

  private def read_hash(size : Int32?, pp) : Nil
    # key_pairs = Array(String).new
    context_save = @context
    @context = nil
    first = true
    consume_map_body(size) { |pairs|
      pp.group do
        if context_save.nil?
          pp.comma unless first
          to_diagnostic_pretty(pairs[0], pp)
          pp.text ": "
          to_diagnostic_pretty(pairs[1], pp)
        else
          field_name = context_save.not_nil![pairs[0].as(Token::IntT).value]
          pp.comma unless first
          pp.text "`#{field_name["name"]}`"
          to_diagnostic_pretty(pairs[0], pp)
          pp.text ": "
          to_diagnostic_pretty(pairs[1], pp)
        end
      end
      first = false if first
    }
  end

  private def consume_array_body(size : Int32?, &block : Token::T ->)
    if size
      size.times do
        token = @lexer.next_token
        raise ParseError.new("Unexpected EOF while reading array body") unless token
        yield token
      end
    else
      loop do
        token = @lexer.next_token
        raise ParseError.new("Unexpected EOF while reading array body") unless token
        break if token.is_a?(Token::BreakT)
        yield token
      end
    end
  end

  private def consume_map_body(size : Int32?, &block : Tuple(Token::T, Token::T) ->)
    if size
      size.times { yield @lexer.next_pair }
    else
      loop do
        key = @lexer.next_token
        raise ParseError.new("Unexpected EOF while reading map key") unless key
        break if key.is_a?(Token::BreakT)

        value = @lexer.next_token
        raise ParseError.new("Unexpected EOF while reading map value") unless value

        yield Tuple.new(key, value)
      end
    end
  end

  private def read_big_int(negative : Bool = false) : String
    token = @lexer.next_token
    raise ParseError.new("Unexpected EOF after tag") unless token
    raise ParseError.new("Unexpected type #{token.class}, want Token::BytesT") unless token.is_a?(Token::BytesT)

    big = BigInt.new(token.value.hexstring, 16)
    if negative
      big *= -1
      big -= 1
    end

    big.to_s
  end

  private def chunks(value : Bytes, chunks : Array(Int32)) : Array(Bytes)
    res = Array(Bytes).new
    bytes = value.to_a
    chunks.each do |size|
      bytes_chunk = bytes.shift(size)
      res << Bytes.new(bytes_chunk.to_unsafe, bytes_chunk.size)
    end
    res
  end

  private def chunks(value : String, chunks : Array(Int32)) : Array(String)
    res = Array(String).new
    arr = value.split("")
    chunks.each do |size|
      res << arr.shift(size).join
    end
    res
  end

  private def bytes(b : Bytes) : String
    "h'#{b.hexstring}'"
  end

  private def string(s : String) : String
    %("#{s}")
  end
end
