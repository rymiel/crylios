module Stelios::Version
  SOURCE = "k"
  PLATFORM = "l"
  PROTOCOL = "w"
  MAJOR = 0
  MINOR = 0
  PATCH = 1
  BUILD = ""

  SHORT = "#{MAJOR}.#{MINOR}.#{PATCH}"
  LONG = "#{SOURCE}#{PLATFORM}#{PROTOCOL}#{VERSION}"
end