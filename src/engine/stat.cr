require "../sban"

module Stelios
  STAT_NAME_LOOKUP = {
    "vit" => Stat::Vit,
    "int" => Stat::Int,
    "dex" => Stat::Dex,
    "str" => Stat::Str,
    "wil" => Stat::Wil,
    "lck" => Stat::Lck,
  }

  enum Stat
    Points
    Level

    Health
    MaxHealth
    Magic
    MaxMagic
    Action
    MaxAction

    Threat

    Vit
    Int
    Dex
    Str
    Wil
    Lck

    Per
    Ini
  end

  private struct StatVal
    @val : UInt16

    def val
      @val.clamp(0, 999)
    end

    def initialize(val : UInt16 = 0_u16)
      @val = val.to_u16
    end

    def to_sban(sban : SBAN::Encoder)
      @val.to_sban(sban)
    end

    def initialize(decoder : SBAN::Decoder)
      @val = UInt16.new decoder
    end
  end

  class StatHash
    getter vals = {} of Stat => StatVal

    def [](key : Stat)
      @vals.[key]?.try &.val || 0_u16
    end

    def []=(key : Stat, val : UInt16)
      @vals[key] = StatVal.new(val)
    end

    def initialize
    end

    def to_sban(sban : SBAN::Encoder)
      @vals.to_sban(sban)
    end

    def initialize(decoder : SBAN::Decoder)
      @vals = Hash(Stat, StatVal).new decoder
    end
  end

  class RawStat < StatHash
    def [](key : Stat)
      case key
      when Stat::Per
        StatVal.new(self[Stat::Int] * 2 + self[Stat::Lck] / 2 + self[Stat::Dex]).val
      when Stat::Ini
        StatVal.new(self[Stat::Dex] + self[Stat::Int] / 1.5).val
      else
        super
      end
    end
  end

  class StatUnion
    include SBAN::Serializable
    @base : StatHash
    @volatile : StatHash

    def initialize(@base, @volatile)
    end

    def [](key : Stat)
      @base[key] + @volatile[key]
    end

    def []=(key : Stat, val : UInt16)
      @base[key] = val
    end

    def []=(key : Symbol | String, val : UInt16)
      @base[STAT_NAME_LOOKUP[key.to_s]] = val
    end

    def to_s
      @base.vals.to_s
    end
  end
end
