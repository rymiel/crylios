class Object
  def to_sban : Bytes
    encoder = SBAN::Encoder.new
    to_sban(encoder)
    encoder.to_slice
  end

  def to_sban(io : IO)
    encoder = SBAN::Encoder.new(io)
    to_sban(encoder)
    self
  end

  def to_sban(encoder : SBAN::Encoder)
    encoder.write(self)
  end
end

struct Set
  def to_sban(encoder : SBAN::Encoder)
    encoder.write_array_start(self.size)
    each { |elem| elem.to_sban(encoder) }
  end
end

class Array
  def to_sban(encoder : SBAN::Encoder)
    encoder.write_array_start(self.size)
    each { |elem| elem.to_sban(encoder) }
  end
end

class Hash
  def to_sban(encoder : SBAN::Encoder)
    encoder.write_object_start(self.size)
    each do |key, value|
      key.to_sban(encoder)
      value.to_sban(encoder)
    end
  end
end

struct Tuple
  def to_sban(encoder : SBAN::Encoder)
    encoder.write_array_start(self.size)
    each { |elem| elem.to_sban(encoder) }
  end
end

struct NamedTuple
  def to_sban(encoder : SBAN::Encoder)
    encoder.write_object_start(self.size)
    {% for key in T.keys %}
      {{key.stringify}}.to_sban(encoder)
      self[{{key.symbolize}}].to_sban(encoder)
    {% end %}
  end
end

struct Enum
  def to_sban(encoder : SBAN::Encoder)
    value.to_sban(encoder)
  end
end

struct Time
  # Encodes the time as a unix timestamp
  def to_sban(encoder : SBAN::Encoder)
    encoder.write(to_unix)
  end
end