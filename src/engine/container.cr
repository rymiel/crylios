require "./item"
require "./io"

module Stelios
  abstract class Container
    abstract def items : Indexable(Item?)
    abstract def add(item : Item)

    def []=(index : Int, val : Item?)
      val.inventory = self
      items[index] = val
    end
  end

  class LinearContainerAddResult
    @entries = [] of {UInt64, UInt64, UInt64}?
    @context : Item

    def initialize(@context)
    end

    def add(slot, initial_count, result_count)
      @entries.push({slot, initial_count, result_count})
    end

    def terminate
      @entries.push nil
    end

    def parse
      analysis = [] of IO::Badge::ContainerAddResult::AddEntry
      terminate = IO::Badge::ContainerAddResult::TerminationReason::None
      # sum = 0
      unless @entries.first
        terminate = IO::Badge::ContainerAddResult::TerminationReason::Full
      else
        @entries.each do |entry|
          unless entry
            terminate = IO::Badge::ContainerAddResult::TerminationReason::NoRoom
          else
            diff = entry[2] - entry[1]
            analysis.push({slot: entry[0], delta_count: diff, result_count: entry[2]})
          end
        end
      end
      IO::Badge::ContainerAddResult.new((IO::Badge::Item.new @context), analysis, terminate)
    end
  end

  abstract class LinearContainer < Container
    getter items : Array(Item?)
    getter size : UInt64

    def initialize(size : Int)
      @size = size.to_u64
      @items = Array(Item?).new(@size, nil)
    end

    alias Snowball = {item: Item, result: LinearContainerAddResult}

    private def raw_add(item : Item, snowball : Snowball? = nil)
      if snowball.nil?
        original = item
        result = LinearContainerAddResult.new item
      else
        original = snowball[:item]
        result = snowball[:result]
      end
      {% for look_for_empty in {false, true} %}
      @items.each_with_index do |iter, i|
        initial_count = 0_u64
        max_count = item.stack
        {% begin %}
        {% unless look_for_empty %}
        if !iter.nil? && item == iter
          next if iter.count >= max_count
          initial_count = iter.count
          max_count = iter.stack
        {% else %}
        if iter.nil?
          iter = item.dup
          iter.count = initial_count
          self[i] = iter
        {% end %}
        else
          next
        end
        {% end %}
        new_count = initial_count + item.count
        overflow = Math.max(0, new_count.to_i64 - max_count).to_u64
        if overflow == 0
          iter.count = new_count
          original = nil
          result.add(i.to_u64, initial_count, new_count)
          return result
        else
          iter.count = max_count
          original.count = overflow
          result.add(i.to_u64, initial_count, iter.count)
          return raw_add(original.dup, {item: original, result: result})
        end
      end
      {% end %}
      result.terminate
      result
    end

    def add(item : Item)
      raw_add(item).parse
    end
  end

  class Inventory < LinearContainer
  end
end
